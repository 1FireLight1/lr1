import os
from dotenv import load_dotenv
from array import *

def insert(arr1, arr2, i):
  arr1 = arr1[:i] + arr2 + arr1[i:]
  return arr1

if __name__ == '__main__':
  load_dotenv()
  try:
    arr1 = eval(os.getenv('arr1'))
    arr2 = eval(os.getenv('arr2'))
    i = int(os.getenv('i'))
  except:
    arr1 = array('i', [1,2,3,4,5])
    arr2 = array('i', [7,8,9])
    i = 3
  print(arr1, arr2, i)
  result = insert(arr1, arr2, i)
  print(result)
  f = open("output/result.txt", "w")
  f.write(str(result))
  f.close()
