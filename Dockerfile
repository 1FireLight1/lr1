FROM python:3.10.4
WORKDIR /lr1
COPY insert.py ./
COPY output/ ./output/
RUN pip install python-dotenv
CMD ["python", "./insert.py"]
